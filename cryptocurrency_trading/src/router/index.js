import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home/index'
import Login from '@/components/Auth/index'
import Register from '@/components/Auth/register'
import Trading from '@/components/Trading/index'
import Wallet from '@/components/Wallet/index'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/trading',
      name: 'Trading',
      component: Trading
    },
    {
      path: '/wallet',
      name: 'Wallet',
      component: Wallet
    }
  ]
})
