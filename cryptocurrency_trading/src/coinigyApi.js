const socketCluster = require('socketcluster-client')

const apiCredentials = {
  apiKey: 'c38ff2f8b18ff5f7eced5e7a07ae019b',
  apiSecret: '8ac7abb75feae980b4a6365142c64da9'
}

const options = {
  hostname: 'sc-02.coinigy.com',
  port: '443',
  secure: 'true'
}

let object = {}

const SCsocket = socketCluster.connect(options)

SCsocket.on('connect', function (status) {
  SCsocket.on('error', function (err) {
    console.log(err)
  })
  SCsocket.emit('auth', apiCredentials, function (err, token) {
    if (!err && token) {
      var scChannel = SCsocket.subscribe('TRADE-GDAX--BTC--USD')
      console.log(scChannel)
      scChannel.watch(function (data) {
        this.price = data.price
        object = data
      })
    } else {
      console.log(err)
    }
  })
})

console.log(object)
